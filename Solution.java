package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class Solution {

	public int[] solution(int K, int M, int[] A) {

		List<Integer> list = new ArrayList();

		// Aplicamos todos los posibles segmentos de longitud K
		for (int i = 0; i + K < A.length; i++) {

			// Obtenemos un stream del array, aumentando en 1 los elementos comprendidos en
			// el segmento ente 0 y K -1.
			IntStream streamBase = Arrays.stream(A, i, K - 1).map(n -> n + 1);

			// Obtenemos una lista solo con los valores existentes en el stream, sin
			// repeticiones
			List<Integer> listDistinct = streamBase.boxed().distinct().collect(Collectors.toList());

			// Convertimos el stream de entrada en una lista para manejarlo en las
			// comprobaciones siguientes
			List<Integer> listBase = streamBase.boxed().collect(Collectors.toList());

			for (Integer item : listDistinct) {
				if (streamBase.filter(n -> n == item).count() % 2 > listBase.size()) {
					list.add(item);
				}
			}

		}
		int[] res = list.stream().distinct().mapToInt(Integer::intValue).toArray();
		return res;
	}

}
