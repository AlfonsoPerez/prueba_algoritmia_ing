import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution {

    public static int[] solution(int K, int M, int[] A) {

        //these variables names are not correct, they should lower case by Java convention
        Set<Integer> leaders = new HashSet<>();

        for (int i = 0; i + K <= A.length; i++) {
            int[] newArrayA = A.clone();
            // +1 to every element of the sequence
            for (int j = i; j < i + K; j++) {
                newArrayA[j]++;
            }
            List<Integer> newArrayAList = Arrays.stream(newArrayA).boxed().collect(Collectors.toList());
            Stream<Map.Entry<Integer, Long>> stream = newArrayAList.stream().collect(Collectors.groupingBy(value -> value, Collectors.counting())).entrySet().stream();
            Optional<Integer> optionalInteger = newArrayAList.stream()
                    .collect(Collectors.groupingBy(value -> value, Collectors.counting()))
                    .entrySet()
                    .stream().max((num1, num2) ->
                            num1.getValue().compareTo(num2.getValue()))
                    .filter(num -> num.getValue() > (newArrayAList.size() / 2))
                    .map(Map.Entry::getKey);
            optionalInteger.ifPresent(integer -> leaders.add(integer));
        }

        return leaders.stream().sorted().mapToInt(Integer::intValue).toArray();
    }

    //The tests should be in a test class
    @Test
    public void getArrayLeaderTestExample1() {
        int[] segment = new int[]{2, 1, 3, 1, 2, 2, 3};
        int[] arrayLeaders = solution(3, 5, segment);
        int[] expectedLeaders = {2, 3};
        Assert.assertArrayEquals(arrayLeaders, expectedLeaders);
    }

    @Test
    public void getArrayLeaderTestExample2() {
        int[] segment = new int[]{1, 2, 2, 1, 2};
        int[] arrayLeaders = solution(4, 2, segment);
        int[] expectedLeaders = {2, 3};
        Assert.assertArrayEquals(arrayLeaders, expectedLeaders);
    }

}

